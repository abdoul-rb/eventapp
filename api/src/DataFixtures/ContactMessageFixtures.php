<?php

namespace App\DataFixtures;

use App\Entity\ContactMessage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ContactMessageFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create("fr_FR");

        // create 5 messages
        for ($i = 0; $i < 10; $i++) {
            $message = new ContactMessage();
            $message->setEmail($faker->email)
                ->setBody($faker->sentence(10))
                ->setIsChecked(false);
            $manager->persist($message);
        }

        $manager->flush();
    }
}
