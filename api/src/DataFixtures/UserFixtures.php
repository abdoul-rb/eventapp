<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $admin = new User();
        $user = new User();

        $admin->setFullname('John Doe')
            ->setEmail('admin@dev.com')
            ->setPseudo('Admin')
            ->setRoles(['ROLE_ADMIN'])
            ->setPassword('password');

        $user->setFullname('Jane Doe')
            ->setEmail('user@dev.com')
            ->setPseudo('User')
            ->setRoles([])
            ->setPassword('password');

        $manager->persist($admin);
        $manager->persist($user);

        $manager->flush();
    }

}
