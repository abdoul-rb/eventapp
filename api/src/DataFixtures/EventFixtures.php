<?php

namespace App\DataFixtures;

use App\Entity\Event;
use App\Entity\EventCategory;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class EventFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {

        $faker = Factory::create("fr_FR");

        // Create some event categories
        for ($i = 0; $i < 8; $i++) {
            $category = new EventCategory();
            $category->setTitle($faker->name)
                ->setDescription($faker->text);
            $manager->persist($category);
        }

        // Finished yesterday
        for ($i = 0; $i < 5; $i++) {
            $event = $this->createEvent(
                $faker->sentence('3'),
                $faker->sentence(),
                $faker->dateTimeBetween('-1 month', '-3 day'),
                $faker->dateTimeBetween('-3 days', '- 1 day'),
                $category
            );
            $manager->persist($event);
        }
        // Is in progress
        for ($i = 0; $i < 1; $i++) {
            $event = $this->createEvent(
                $faker->sentence('3'),
                $faker->sentence(),
                $faker->dateTimeBetween('-1 day', '- 1 hour'),
                $faker->dateTimeBetween('+1 hour', '+1 day'),
                $category
            );
            $manager->persist($event);
        }
        // Begin tomorow
        for ($i = 0; $i < 5; $i++) {
            $event = $this->createEvent(
                $faker->sentence('3'),
                $faker->sentence(),
                $faker->dateTimeBetween('+3 days', '+5 days'),
                $faker->dateTimeBetween('+5 days', '+7 days'),
                $category
            );
            $manager->persist($event);
        }

        $manager->flush();
    }

    public function createEvent(
        string $title,
        string $description,
        Datetime $beginAt = null,
        Datetime $finishAt = null,
        EventCategory $category,
        bool $published = true
    ): Event {
        return (new Event())
            ->setTitle($title)
            ->setDescription($description)
            ->setBeginAt($beginAt)
            ->setFinishAt($finishAt)
            ->setPublished($published)
            ->addCategory($category);
    }
}
