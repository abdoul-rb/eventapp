<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MeController extends AbstractController
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function __invoke(): ?User
    {
        $token = $this->tokenStorage->getToken();

        if(!$token) {
            return null;
        }

        $user = $token->getUser();

        if(!$user instanceof User) {
            return null;
        }

        return $user;
    }
};
