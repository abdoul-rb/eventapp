<?php

namespace App\Services;

use Exception;
use Monolog\Handler\AbstractProcessingHandler;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class LoggerHandler extends AbstractProcessingHandler
{
    protected function write(array $record): void
    {
        $context = stream_context_create([
            'http' => [
                'method' => 'POST',
                'header' => 'Content-Type: application/json',
                'content' => json_encode([
                    'level' => $record['level_name'],
                    'message' => $record['message'],
                    'source' => 'Symfony App'
                ]),
                'ignore_errors' => true
            ]
        ]);

        file_get_contents('http://logger:3003/logs', false, $context);

        if (strpos($http_response_header[0], '201') === false) {
            throw new Exception(json_encode($http_response_header));
        }
    }
}
