<?php

namespace App\EventListener;

use App\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordEncoderSubscriber implements EventSubscriber
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preUpdate
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof User) {
            $this->encodePassword($entity);
        }
    }

    public function preUpdate(PreUpdateEventArgs $args): void
    {

        $entity = $args->getObject();

        // If the password from a user is updated, we encode it
        if ($entity instanceof User && $args->hasChangedField('password')) {
            $this->encodePassword($entity);
        }
    }

    private function encodePassword(User $entity): void
    {
        $entity->setPassword($this->encoder->encodePassword($entity, $entity->getPassword()));
    }
}
