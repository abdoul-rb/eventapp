<?php

namespace App\Entity;

use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=EventRepository::class)
 * @ApiResource(
 *      normalizationContext={"groups"={"read"}},
 *      denormalizationContext={"groups"={"write"}}
 * )
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("title")
 * @ApiFilter(SearchFilter::class, properties={"title": "ipartial", "description": "ipartial"})
 * @ApiFilter(DateFilter::class, properties={"beginAt": DateFilter::EXCLUDE_NULL, "finishAt": DateFilter::EXCLUDE_NULL})
 * @ApiFilter(BooleanFilter::class, properties={"published"})
 * @ApiFilter(OrderFilter::class, properties={"id", "beginAt", "finishAt", "created_at"}, arguments={"orderParameterName"="order"})
 */
class Event
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true, length=80)
     * @Assert\Length(
     *      min = 3,
     *      max = 80,
     *      minMessage="event.title.minMessage",
     *      maxMessage="event.title.maxMessage"
     * )
     * @Groups({"read", "admin:write"})
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="event.description.not_blank")
     * @Groups({"read", "admin:write"})
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message="event.begin_at.not_blank")
     * @Groups({"read", "admin:write"})
     */
    private $beginAt;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message="event.finish_at.not_blank")
     * @Groups({"read", "admin:write"})
     */
    private $finishAt;

    /**
     * @ORM\Column(type="string", length=2048, nullable=true, options={"default" : null})
     * @Assert\Type({"string", "null"})
     * @Assert\Length(
     *      min = 1,
     *      max = 2048
     * )
     * @Groups({"read", "admin:write"})
     */
    private $picture_attached = null;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\Type("bool", message="event.published.type")
     * @Groups({"read", "admin:write"})
     */
    private $published;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read"})
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read"})
     */
    private $created_at;

    /**
     * @ORM\ManyToMany(targetEntity=EventCategory::class, inversedBy="events")
     * @Groups({"read", "admin:write"})
     */
    private $categories;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getBeginAt(): ?\DateTimeInterface
    {
        return $this->beginAt;
    }

    /**
     * @SerializedName("beginAt")
     */
    public function setBeginAt(\DateTimeInterface $beginAt): self
    {
        $this->beginAt = $beginAt;

        return $this;
    }

    /**
     * @SerializedName("finishAt")
     */
    public function getFinishAt(): ?\DateTimeInterface
    {
        return $this->finishAt;
    }

    public function setFinishAt(\DateTimeInterface $finishAt): self
    {
        $this->finishAt = $finishAt;

        return $this;
    }

    public function getPictureAttached(): ?string
    {
        return $this->picture_attached;
    }

    public function setPictureAttached(?string $picture_attached): self
    {
        $this->picture_attached = $picture_attached;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * @return Collection|EventCategory[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(EventCategory $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(EventCategory $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
        }

        return $this;
    }
}
