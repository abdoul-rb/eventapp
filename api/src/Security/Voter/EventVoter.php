<?php

namespace App\Security\Voter;

use App\Entity\Event;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Anonymous can read event to only see if it's public
 * @package App\Security\Voter
 */
class EventVoter extends Voter
{
    private $security = null;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, ['ANONYMOUS_READ_EVENT'])
            && $subject instanceof \App\Entity\Event;
    }

    // Voter for anonymous users read only event pusblished
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if(!$user instanceof UserInterface) {
            if($subject->getPublished()) {
                return true;
            }
            return false;
        } else {
            if($this->security->isGranted('ROLE_ADMIN', $user)) {
                return true;
            }
            return false;
        }
    }
}
