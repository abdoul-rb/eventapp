ARG PHP_VERSION=7.4

# "php" stage
FROM php:${PHP_VERSION}-fpm-alpine AS api_platform_php

RUN apk update && apk upgrade
RUN apk add nginx

RUN mkdir -p /var/cache/nginx /run/nginx

RUN ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log

RUN apk add --no-cache \
		acl \
		fcgi \
		file \
		gettext \
		git \
		gnu-libiconv \
	;
ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so

RUN set -eux; \
	apk add --no-cache --virtual .build-deps \
		$PHPIZE_DEPS \
		icu-dev \
		libzip-dev \
		postgresql-dev \
		zlib-dev \
	; \
	\
	docker-php-ext-configure zip; \
	docker-php-ext-install -j$(nproc) \
		intl \
		pdo_pgsql \
		zip \
	; \
	pecl install \
		apcu-5.1.19 \
	; \
	pecl clear-cache; \
	docker-php-ext-enable \
		apcu \
		opcache \
	; \
	\
	runDeps="$( \
		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
			| tr ',' '\n' \
			| sort -u \
			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
	)"; \
	apk add --no-cache --virtual .api-phpexts-rundeps $runDeps; \
	\
	apk del .build-deps

ENV NODE_VERSION 16.13.1

RUN addgroup -g 1000 node \
    && adduser -u 1000 -G node -s /bin/sh -D node \
    && apk add --no-cache \
        libstdc++ \
    && apk add --no-cache --virtual .build-deps \
        curl \
    && ARCH= && alpineArch="$(apk --print-arch)" \
      && case "${alpineArch##*-}" in \
        x86_64) \
          ARCH='x64' \
          CHECKSUM="3b4c47e5554fa466651a767691fc76c09b6a514b49d79bbd0061e549614adedf" \
          ;; \
        *) ;; \
      esac \
  && if [ -n "${CHECKSUM}" ]; then \
    set -eu; \
    curl -fsSLO --compressed "https://unofficial-builds.nodejs.org/download/release/v$NODE_VERSION/node-v$NODE_VERSION-linux-$ARCH-musl.tar.xz"; \
    echo "$CHECKSUM  node-v$NODE_VERSION-linux-$ARCH-musl.tar.xz" | sha256sum -c - \
      && tar -xJf "node-v$NODE_VERSION-linux-$ARCH-musl.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
      && ln -s /usr/local/bin/node /usr/local/bin/nodejs; \
  else \
    echo "Building from source" \
    # backup build
    && apk add --no-cache --virtual .build-deps-full \
        binutils-gold \
        g++ \
        gcc \
        gnupg \
        libgcc \
        linux-headers \
        make \
        python3 \
    # gpg keys listed at https://github.com/nodejs/node#release-keys
    && for key in \
      4ED778F539E3634C779C87C6D7062848A1AB005C \
      94AE36675C464D64BAFA68DD7434390BDBE9B9C5 \
      74F12602B6F1C4E913FAA37AD3A89613643B6201 \
      71DCFD284A79C3B38668286BC97EC7A07EDE3FC1 \
      8FCCA13FEF1D0C2E91008E09770F7A9A5AE15600 \
      C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8 \
      C82FA3AE1CBEDC6BE46B9360C43CEC45C17AB93C \
      DD8F2338BAE7501E3DD5AC78C273792F7D83545D \
      A48C2BEE680E841632CD4E44F07496B3EB3C1762 \
      108F52B48DB57BB0CC439B2997B01419BD92F80A \
      B9E2F5981AA6E0CD28160D9FF13993A75599653C \
    ; do \
      gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys "$key" || \
      gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "$key" ; \
    done \
    && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION.tar.xz" \
    && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc" \
    && gpg --batch --decrypt --output SHASUMS256.txt SHASUMS256.txt.asc \
    && grep " node-v$NODE_VERSION.tar.xz\$" SHASUMS256.txt | sha256sum -c - \
    && tar -xf "node-v$NODE_VERSION.tar.xz" \
    && cd "node-v$NODE_VERSION" \
    && ./configure \
    && make -j$(getconf _NPROCESSORS_ONLN) V= \
    && make install \
    && apk del .build-deps-full \
    && cd .. \
    && rm -Rf "node-v$NODE_VERSION" \
    && rm "node-v$NODE_VERSION.tar.xz" SHASUMS256.txt.asc SHASUMS256.txt; \
	fi \
	&& rm -f "node-v$NODE_VERSION-linux-$ARCH-musl.tar.xz" \
	&& apk del .build-deps \
	# smoke tests
	&& node --version \
	&& npm --version
RUN apk --no-cache add pkgconfig autoconf automake libtool nasm build-base zlib-dev libpng-dev git supervisor
RUN npm install -g npm@8.1.4

COPY --from=composer:1.8 /usr/bin/composer /usr/bin/composer
COPY ./docker/nginx/container-files /
COPY ./docker/php/container-files /

RUN ln -s $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini

RUN adduser -D -G www-data noroot

RUN chown -R noroot:www-data /var/cache/nginx && \
    chown -R noroot:www-data /var/lib/nginx && \
    chown -R noroot:www-data /var/log/nginx && \
    chown -R noroot:www-data /etc/nginx/

ENV PATH="${PATH}:/home/noroot/.composer/vendor/bin"
# To bypass Symfony cache:clear command (at end of composer's execution)
ENV DATABASE_URL=""
ARG APP_ENV=prod
ARG VUE_APP_API_URL

# manually create pid file
RUN touch /run/nginx/nginx.pid && \
    chown -R noroot:www-data /run/nginx/nginx.pid

RUN rm $PHP_INI_DIR/conf.d/api-platform.dev.ini
RUN sed -i "s/ENVIRONMENT/${APP_ENV}/g" /usr/local/etc/php-fpm.conf
RUN sed -i "s/ENVIRONMENT/${APP_ENV}/g" /etc/nginx/nginx.conf

COPY api /home/noroot/api
COPY client /home/noroot/client

RUN chown -R noroot:www-data /home/noroot/api && \
    chown -R noroot:www-data /home/noroot/client

COPY docker/supervisord.conf /etc/supervisord.conf
COPY run.deploy.sh /run.deploy.sh

USER noroot

RUN cd /home/noroot/api && touch .env && composer install --no-dev
RUN cd /home/noroot/client && npm ci && npm run build && rm -rf node_modules

# TODO write into script instructions below + add migrations / schema update execution (before supervisord)
CMD ["/run.deploy.sh"]
