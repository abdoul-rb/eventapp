# Environnement de production

Le Dockerfile utilisé est `deploy.Dockerfile`.

Ce Dockerfile nous construit une image ayant les services suivants :

- `PHP - PHP-FPM`
- `Nginx`
- `Node` (uniquement pour le build du Front)

## Build

Au moment du build (à la création de l'image docker via un `docker build`), plusieurs variables d'environnements sont utilisées :

- APP_ENV (par défaut possède la valeur `prod`)
- VUE_APP_API_URL (par défaut, est vide)
  - Nécessaire au front, pour pouvoir solliciter l'API.

Pour assigner une valeur à chaque variable, il faut les **passer** dans la construction de l'image :

```sh
docker build . -t <repository/name> --build-arg APP_ENV=<nouvelle valeur> --build-arg VUE_APP_API_URL=<nouvelle valeur> -f deploy.Dockerfile
```
**Remarque :** les nouvelles valeurs devront être passées via les secrets du CI/CD que GitLab fournit au moment du déploiement.

## Run

Pour rappel, le run est l'étape qui se déroule au **lancement du container**.

Les variables utilisées sont les suivantes :

- `DATABASE_URL`, pour se connecter au serveur de la BDD
- `PORT`, le port sur lequel le serveur Nginx tourne
- `APP_ENV`, l'environnement de l'application

Heroku fournit automatiquement ces 3 variables au lancement du container, via son container registry. Mais il est possible d'assigner des valeurs manuellement, si on souhaite mener des tests :

### Test de la prod en local

**Build**
```sh
docker build . -t abdoul-rb/nginx-php-vue --build-arg VUE_APP_API_URL=http://localhost:8999 -f deploy.Dockerfile
docker build . -t abdoul-rb/nginx-php-vue --build-arg VUE_APP_API_URL=http://localhost:8999 -f deploy.Dockerfile --no-cache
```

**Run**

Assurez vous d'avoir le réseau `eventapp_default` déjà ouvert pour pouvoir faire communiquer votre container à la base de données du container lancé localement. Car votre image ne contient pas de service pour la BDD.

```sh
docker run -d -p 8111:8222 -e PORT=8222 -e DATABASE_URL=postgresql://db -e APP_ENV=prod <image>
docker run -p 8999:8222 -e PORT=8222 -e APP_ENV=prod -e DATABASE_URL='postgresql://api-platform:!ChangeMe!@db:5432/api' --name nginx-php-vue-app --network=eventapp_default abdoul-rb/nginx-php-vue
```

## On Heroku

[Pushing existing image](https://devcenter.heroku.com/articles/container-registry-and-runtime#pushing-an-existing-image)

**Build de l'image**
Utilisation de la CLI

```sh
docker build . -t registry.heroku.com/ponies-app/web --iidfile image_id.txt --build-arg VUE_APP_API_URL=https://we-ponies.herokuapp.com -f deploy.Dockerfile --no-cache
```

`--iidfile`: cette option permet de stocker l'ID de l'image dans un fichier, ici le fichier `image_id.txt`

`--build-arg`: défini la variable d'environnement `VUE_APP_API_URL` lors du build


**Pusher votre image sur Heroku**
La syntaxe est la suivante : `docker tag <image> registry.heroku.com/<app>/<process-type>`

```sh
docker push registry.heroku.com/ponies-app/web
```

**API**
Passer par l'API de Heroku pour exécuter.
[API Docs](https://devcenter.heroku.com/articles/container-registry-and-runtime#api)

```sh
   curl --netrc -X PATCH https://api.heroku.com/apps/ponies-app/formation \
  -d '{
  "updates": [
    {
      "type": "web",
      "docker_image": "'$IMAGE_ID'"
    }
  ]
}' \
  -H "Content-Type: application/json" \
  -H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
  -H "Authorization: Bearer $HEROKU_API_KEY"
```

**Voir les logs de l'application**
`heroku logs --tail --app=we-ponies`
