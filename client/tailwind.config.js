const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  mode: 'jit',
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ['Nunito', ...defaultTheme.fontFamily.sans],
        atma: ['Atma'],
        poppins: ['Poppins']
      },
      colors: {
        hk: {
          purple: "#627FFF",
          yellow: "#F0C403",
          white: "#FFFFFF",
          gray: "#919293"
        },
        discord: {
          purple: "#7289DA"
        }
      },
      inset: {
        '35/100': "35%"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
}