import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/frontend/Home/Index.vue";
import axios from "axios";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "Home",
        component: Home
    },
    {
        path: "/evenements",
        name: "events",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import(
                /* webpackChunkName: "about" */ "@/views/frontend/Events.vue"
            )
    },
    {
        path: "/evenements/:id",
        name: "events.show",
        component: () => import("@/views/frontend/ShowEvent.vue")
    },
    {
        path: "/planning",
        name: "planning",
        component: () => import("@/views/frontend/Planning.vue")
    },
    {
        path: "/streaming",
        name: "streaming",
        component: () => import("@/views/frontend/Streaming.vue")
    },
    {
        path: "/mention-legales",
        name: "legalMentions",
        component: () => import("@/views/frontend/LegalMentions.vue")
    },
    {
        path: "/auth",
        component: () => import("@/views/layouts/GuestLayout.vue"),
        children: [
            {
                path: "login",
                name: "login",
                component: () => import("@/views/guest/Login.vue")
            },
            {
                path: "register",
                name: "register",
                component: () => import("@/views/guest/Register.vue")
            },
            {
                path: "/forgot-password",
                name: "forgot-password",
                component: () => import("@/views/guest/ForgotPassword.vue")
            }
        ],
        beforeEnter: (to, from, next) => {
            // If logged in, redirect to home
            if (localStorage.getItem("token")) {
                next({ name: "Home" });
            } else {
                next();
            }
        }
    },
    {
        path: "/staff",
        component: () => import("@/views/layouts/AdminLayout.vue"),
        children: [
            {
                path: "users",
                name: "admin.users.index",
                component: () => import("@/views/admin/users/UsersListing.vue")
            },
            {
                path: "users/create",
                name: "admin.users.create",
                component: () => import("@/views/admin/users/Create.vue")
            },
            {
                path: "users/edit/:id",
                name: "admin.users.edit",
                component: () => import("@/views/admin/users/Edit.vue")
            },
            {
                path: "events",
                name: "admin.events.index",
                component: () => import("@/views/admin/events/Index.vue")
            },
            {
                path: "events/create",
                name: "admin.events.create",
                component: () => import("@/views/admin/events/Create.vue")
            },
            {
                path: "events/:id/edit",
                name: "admin.events.edit",
                component: () => import("@/views/admin/events/Edit.vue")
            },
            {
                path: "event-categories",
                name: "admin.eventCategories.index",
                component: () =>
                    import("@/views/admin/eventCategories/Index.vue")
            },
            {
                path: "user-messages/need-check",
                name: "admin.userMessages.needCheck",
                component: () =>
                    import("@/views/admin/userMessages/NeedCheck.vue")
            },
            {
                path: "user-messages/checked",
                name: "admin.userMessages.checked",
                component: () =>
                    import("@/views/admin/userMessages/Checked.vue")
            }
        ],
        beforeEnter: (to, from, next) => {
            if (!localStorage.getItem("token")) {
                next({ name: "login" });
            } else {
                next();
            }
        }
    },
    {
        path: "/",
        component: () => import("@/views/layouts/ProfileLayout.vue"),
        children: [
            {
                path: 'profile',
                name: 'profile',
                component: () => import('@/views/profile/Index.vue')
            }
        ],
        beforeEnter: (to, from, next) => {
            if (!localStorage.getItem("token")) {
                next({ name: "login" });
            } else {
                next();
            }
        }

    },
    {
        path: "/internal-error",
        name: "InternalError",
        component: () => import("@/views/frontend/InternalError.vue")
    },
    {
        path: "/*",
        name: "NotFound",
        component: () => import("@/views/frontend/NotFound.vue")
    }
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

axios.interceptors.request.use(
    function(config) {
        // Use automatically the user JWT token if it exists
        if (localStorage.getItem("token")) {
            config.headers.Authorization = `Bearer ${localStorage.getItem(
                "token"
            )}`;
        }
        // Change the default content type for api platform
        if (config.method === "post") {
            config.headers["Content-Type"] = "application/ld+json";
        }
        if (config.method === "patch") {
            config.headers["Content-Type"] = "application/merge-patch+json";
        }
        return config;
    },
    function(error) {
        return Promise.reject(error);
    }
);

// Redirect automatically the user when have an error 401
axios.interceptors.response.use(
    function(response) {
        return response;
    },
    function(error) {
        if (error.response.status === 401) {
            if (localStorage.getItem("token")) {
                localStorage.removeItem("token");
                router.push({ name: "login" });
            }
        }
        return Promise.reject(error);
    }
);

export default router;
