import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import "./assets/index.css";
import Dayjs from "vue-dayjs";
import VCalendar from "v-calendar";
import FlashMessage from "@smartweb/vue-flash-message";

Vue.config.productionTip = false;

Vue.use(Dayjs, {
    lang: "fr"
});
Vue.use(VCalendar, {
    componentPrefix: "v", // If set at vc, use <vc-calendar /> instead of <v-calendar />
    locales: {
        fr: {
            firstDayOfWeek: 2,
            masks: {
                L: "DD-MM-YYYY"
            }
        }
    }
});
Vue.use(FlashMessage, {
    time: 3000
});

Vue.mixin({
    // create is dark computed
    computed: {
        isDarkTheme: function() {
            return (
                window.matchMedia !== undefined &&
                window.matchMedia("(prefers-color-scheme: dark)").matches
            );
        }
    },
    methods: {
        copyToClipboard(message) {
            if (navigator.clipboard) {
                // Recent Clipboard API
                navigator.clipboard.writeText(message);
                console.log("Copied to clipboard");
            } else {
                // Old method which work on all browsers (but is deprecated)
                const el = document.createElement("textarea");
                el.value = message;
                document.body.appendChild(el);
                el.select();
                document.execCommand("copy");
                document.body.removeChild(el);
            }
        }
    }
});

new Vue({
    router,
    render: h => h(App)
}).$mount("#app");
