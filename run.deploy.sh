#!/bin/sh

sed -i 's/$PORT/'"$PORT"'/g' /etc/nginx/http.d/prod.conf
cd /home/noroot/api
bin/console doctrine:schema:update --dump-sql --force
/usr/bin/supervisord
