# API Platform

## Manage submodules
### At git clone
`git clone --recursive $URL`

### When someone else added a new submodule
`git submodule update --init`

### When you've added the submodule but you'd like to update its content
`git submodule foreach git pull`
## Other

**Port Client VueJS**
[localhost:8O80](http://localhost:8O80)

**Swagger docs**
[https://localhost:8443/docs](https://localhost:8443/docs)

**Port API**
[localhost:](http://localhost:)

**Launch Docker**
`docker-compose build --no-cache`
`docker-compose up -d`

**Entité avec API Platform**
`docker-compose exec php bin/console make:entity`

**Drop database**
`docker-compose exec php bin/console d:s:d --force`

**Migrations**
**Create and run migrations**
`docker-compose exec php bin/console make:migration`
`docker-compose exec php bin/console doctrine:migrations:migrate`

**Update database**
`docker-compose exec php bin/console d:s:u --dump-sql`
`docker-compose exec php bin/console d:s:u --force`

**Create from Symfony project**
`composer create-project symfony/skeleton <project-name>`
`composer require api`

**JWT**
[https://api-platform.com/docs/core/jwt/](https://api-platform.com/docs/core/jwt/)

`docker-compose exec php composer require jwt-auth -W`

**Generate auth user**
`docker-compose exec php bin/console make:user`

**Watch npm compilation**
`docker-compose exec client npm run serve`

**Install the dependancies**
`docker-compose exec client npm install`
`docker-compose exec php composer install`

Pas oublier de dump la base de données d:s:u

setfact: change les droits sur les fichiers par rapport au user courant

Faire un listener pour encoder le mot de passe
`docker-compose exec php security:encode` `s:en`

**Installation des extensions Symfony**
`docker-compose exec php composer require stof/doctrine-extensions-bundle:1.5.0`

## Accéder à la base de données
Lancez `docker-compose exec db psql -U api-platform api`


## CI/CD Gitlab

Dans le fichier Caddyfile, on passe par une communication reseau vers le service PHP au lieu de passer par des sockets, pour auto reload les volumes de notre container
Dans le zz-docker.conf on ecoute le service PHP-FPM

On ajoute un Dockerfile propre à la dev pour le services PHP du docker-compose.yml

docker-compose.yml volumes
Les services php-fpm et caddy ne se trouvent pas sur le même hôte et le fichier de socker n'est pas immédiatement disponible dans caddy a chaque relancement du conatiner, privilegié une comm réseau/TCP (php_socket:/var/run/php)

## Docs

Dans le fichier de configuration d'API Platform pour la prod, on a mis le prefix `api` : comme le back et le front passent par le même serveur, on dit au serveur que ces deux couches soient appelées toutes les deux avec `/`.


## VueJS Formik
[https://formik.org/](https://formik.org/)

Get de la data avec Formik. Connecter la balise Field et la balise Formik.

Un composant **Field** qui va linker un composant de base qui va renvoyer toutes les valeurs du formulaires. Doit générer un `input` avec le bon type (email, password, textarea, etc), ou un composant custom, `div`, avec un dynamique component.

Une balise **Formik** qui va contenir des fields, donc aura un `slot`, qui contient toute la data, et propage cette data au balise **Field**.

Avoir des **events customs** et faire de **l'injection de dépendance**.

## Docs

Dans le fichier de configuration d'API Platform pour la prod, on a mis le prefix `api` : comme le back et le front passent par le même serveur, on dit au serveur que ces deux couches soient appelées toutes les deux avec `/`.

## Packages
### Vuejs
- Vcalendar : https://vcalendar.io/
- vue-jwt-decode : https://www.npmjs.com/package/vue-jwt-decode
- Flash messages : https://vuejsexamples.com/single-flash-message-with-vue/

### NodeJs
- Rate limiter : https://www.npmjs.com/package/express-rate-limit
- Needle : https://www.npmjs.com/package/needle
