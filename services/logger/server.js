const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const Logs = require('./models/Logs');
const mongoose = require('mongoose');
const app = express();

app.use(cors());
app.use(bodyParser.json());

mongoose.connect(process.env.MONGO_URL);

app.post('/logs', (req, res) => {
    new Logs(req.body)
        .save()
        .then(data => res.status(201).json(data))
        .catch(error => res.status(500).json(error));
});

app.get('/logs', (req, res) => {
    Logs.find()
        .then(data => res.status(200).json(data))
        .catch(error => res.status(500).json(error));
});

const port = process.env.NODE_PORT || '3005';
app.listen(port, () => console.log(`Server listening on port ${port}`));
