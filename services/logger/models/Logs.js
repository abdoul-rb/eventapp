const mongoose = require('mongoose');

const LogsSchema = new mongoose.Schema({
    date: {
        type: Date,
        default: Date.now
    },
    level: String,
    message: String,
    source: String
});

const Logs = mongoose.model('Logs', LogsSchema);

module.exports = Logs;