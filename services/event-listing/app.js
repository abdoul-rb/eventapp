const express = require("express");
const app = express();
const serverPort = process.env.NODE_PORT || 3000;
const cors = require('cors');

// Use the body-parser middleware
app.use(express.json());

// Set all routes to application/json format
app.set("Content-Type", "application/json");

// Enable CORS
app.use(cors())

// Import route files
const eventRoutes = require("./routes.js");
app.use("/events", eventRoutes);

// Start the server
app.listen(serverPort, () => {
  console.log(`Example app listening at http://localhost:${serverPort}`);
});
