const express = require("express");
const router = express();
const needle = require("needle");
const API_URL = process.env.API_URL || "http://api";

// Get top event(s)
router.get("/top", (serverRequest, serverResponse) => {
  let bodyParsed;
  let events;
  const maxItemsToShow = Number(serverRequest.query.maxItemsToShow) || 3;

  // Request the api
  needle(
    "get",
    `${API_URL}/events?
		published
		&beginAt[strictly_before]=now
		&finishAt[strictly_after]=now
		&order[beginAt]=asc
		&itemsPerPage=${maxItemsToShow}
		&page=1`
  )
    .then((res) => {
      // !!! We use this to parse the response because all @ characters are escaped with the needle parser
      bodyParsed = JSON.parse(res.body);
      events = bodyParsed["hydra:member"];

      // If the "events in progress" list is not empty
      if (events.length > 0) {
        // has an event in progress
        serverResponse.send({
          events: events,
          hasAnEventInProgress: true,
          hasMorePages: bodyParsed["hydra:view"]["hydra:next"] !== undefined,
        });
      } else {
        // has not an event in progress
        needle(
          "get",
          `${API_URL}/events?
		  		published
				&beginAt[strictly_after]=now
				&order[beginAt]=asc
				&itemsPerPage=1
				&page=1`
        ).then((res) => {
          bodyParsed = JSON.parse(res.body);

          // Send the future event(s)
          serverResponse.send({
            events: bodyParsed["hydra:member"],
            hasAnEventInProgress: false,
            hasMorePages: bodyParsed["hydra:view"]["hydra:next"] !== undefined,
          });
        });
      }
    })
    .catch((err) => {
      console.error(err);
      serverResponse.status(500);
      serverResponse.send("Internal server error");
    });
});

// Get past event(s)
router.get("/past", (serverRequest, serverResponse) => {
  const maxItemsToShow = Number(serverRequest.query.maxItemsToShow) || 3;

  // Request the api
  needle(
    "get",
    `${API_URL}/events?
		published
		&finishAt[strictly_before]=now
		&order[beginAt]=desc
		&itemsPerPage=${maxItemsToShow}
		&page=1`
  )
    .then((res) => {
      const bodyParsed = JSON.parse(res.body);
      // Send the past event(s)
      serverResponse.send({
        events: bodyParsed["hydra:member"],
        hasMorePages: bodyParsed["hydra:view"]["hydra:next"] !== undefined,
      });
    })
    .catch((err) => {
      console.error(err);
      serverResponse.status(500);
      serverResponse.send("Internal server error");
    });
});

// Get future event(s)
router.get("/future", (serverRequest, serverResponse) => {
  let maxItemsToShow = Number(serverRequest.query.maxItemsToShow) || 3;
  let bodyParsed;
  let events;
  let showFutureEventAtTop = false;

  // Check if there is an event in progress
  needle(
    "get",
    `${API_URL}/events?
		published
		&beginAt[strictly_before]=now
		&finishAt[strictly_after]=now
		&order[beginAt]=asc
		&itemsPerPage=1
		&page=1`
  )
    .then((res) => {
      events = JSON.parse(res.body)[`hydra:member`];
      // If the "events in progress" list is empty, we'll show at top the first future event
      // So we'll also need to get another future event in the specific list
      if (events.length == 0) {
        showFutureEventAtTop = true;
        maxItemsToShow += 1;
      }

      needle(
        "get",
        `${API_URL}/events?
			published
			&beginAt[strictly_after]=now
			&order[beginAt]=asc
			&itemsPerPage=${maxItemsToShow}
			&page=1`
      ).then((res) => {
        bodyParsed = JSON.parse(res.body);
        events = bodyParsed["hydra:member"];

        // If there is not any "in progress" event
        if (showFutureEventAtTop) {
          events.shift();
        }

        // Send the future event(s)
        serverResponse.send({
          events: events,
          hasMorePages: bodyParsed["hydra:view"]["hydra:next"] !== undefined,
        });
      });
    })
    .catch((err) => {
      console.error(err);
      serverResponse.status(500);
      serverResponse.send("Internal server error");
    });
});

module.exports = router;
