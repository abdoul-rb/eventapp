const express = require("express");
const router = express();
const needle = require("needle");
var dayjs = require('dayjs')
const API_URL = process.env.API_URL || "http://api";

// Get planning events
router.get("/events", (serverRequest, serverResponse) => {
  // Request the api
  needle(
    "get",
    `${API_URL}/events?
		published
		&order[beginAt]=desc
    &order[id]=asc
    &pagination=false`
  )
    .then((res) => {
      const bodyParsed = JSON.parse(res.body);
      /**
       * @var {Array} events A list of well formated events for the planning
       */
      let events = [];

      // For each 'hydra:member' add formatted event in an array
      bodyParsed["hydra:member"].forEach((event) => {
        // Create easy date objects
        let beginDate = dayjs(event.beginAt);
        let finishDate = dayjs(event.finishAt);

        // Get a range of all day when an event appears
        const getDateRange = function(beginDate, finishDate) {
          let dateRange = [];
          let date = beginDate;
          while (date <= finishDate) {
            dateRange.push(date.format("YYYY-MM-DD"));
            date = date.add(1, "day");
          }
          return dateRange;
        }

        // Create a list of event resources for planning show
        events.push({
          key: event.id,
          dates: getDateRange(beginDate, finishDate),
          customData: {
            title: event.title,
            startDay: beginDate.format('D'),
            endDay: finishDate.format('D'),
            startTime: beginDate.format('HH:mm'),
            endTime: finishDate.format('HH:mm'),
            // bgColor: event.bgColor,
            // textColor = event.textColor
          }
        });
      });

      // Send the events with well formatted data
      serverResponse.send(events);
    })
    .catch((err) => {
      console.error(err);
      serverResponse.status(500);
      serverResponse.send("Internal server error");
    });
});

module.exports = router;
