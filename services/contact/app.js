const express = require("express");
const app = express();
const serverPort = process.env.NODE_PORT || 3000;
const mongoose = require("mongoose");
const cors = require('cors');

// Connect to the mongo database
// mongoose.connect(process.env.MONGODB_URI || "mongodb://localhost/blacklist");

// Use the body-parser middleware
app.use(express.json());

// Set all routes to application/json format
app.set("Content-Type", "application/json");

// Enable CORS
app.use(cors())

// Import route files
const eventRoutes = require("./routes.js");
app.use(eventRoutes);

// Start the server
app.listen(serverPort, () => {
  console.log(`Example app listening at http://localhost:${serverPort}`);
});
