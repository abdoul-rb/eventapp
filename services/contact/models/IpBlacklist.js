const mongoose = require("mongoose");

const IpBlacklistSchema = new mongoose.Schema({
  ip: {
    type: String,
    required: true,
  },
  warning_count: {
    type: Number,
    default: 0,
  },
  last_reason: {
    type: String,
  },
  created_at: {
    type: Date,
    default: Date.now,
  },
  updated_at: {
    type: Date,
    default: Date.now,
  },
});

const IpBlacklist = mongoose.model("IpBlacklist", IpBlacklistSchema);

module.exports = IpBlacklist;