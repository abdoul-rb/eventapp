const express = require("express");
const router = express();
const needle = require("needle");
const API_URL = process.env.API_URL || "http://api";
const rateLimit = require("express-rate-limit");
const { body, validationResult } = require("express-validator");
const IpBlacklist = require("./models/IpBlacklist");

// Create a global rate limiter
const limiter = rateLimit({
  windowMs: 60, // 1 minute
  max: 3, // Limit each IP to 10 requests per `window` (here, per minute)
  standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
  legacyHeaders: false, // Disable the `X-RateLimit-*` headers
  handler: function (req, res, next) {
    // The user has exceeded their rate limit, enter its ip addres into the blacklist database
    // So first step : Search the ip address of the user in the database and update it or store a new one
    IpBlacklist.findOne({ ip: req.ip })
      .then((ipBlacklist) => {
        // If the ip is found in the database, increment the warning count and update the last_reason
        if (ipBlacklist) {
          ipBlacklist.warning_count++;
          ipBlacklist.last_reason = "Abused the contact form too many times";
          ipBlacklist.updated_at = Date.now();

          // Save the ip to the database
          ipBlacklist.save().catch((err) => {
            console.log(err);
            res.status(500).res("Internal server error");
          });

          // If the ip has 3 warns, return error
          if(ipBlacklist.warning_count >= 3) {
            res.status(403).res("You've been blacklisted");
          }
        } else {
          // If the ip is not found in the database, create a new ip entry
          var newIpBlacklist = new IpBlacklist({
            ip: req.ip,
            warning_count: 1,
            last_reason: "Abused the contact form too many times",
          });

          // Save the ip to the database
          newIpBlacklist.save().catch((err) => {
            console.log(err);
            res.status(500).res("Internal server error");
          });
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(500).res("Internal server error");
      });

    // Then respond with a 429 status code and the text `Too Many Requests`
    res.status(429).send({
      error: "Too Many Requests",
    });
  },
});

// Retieve the contactMessage argument from the request body
router.post(
  "/send-message",
  limiter,
  body("email").isEmail().withMessage("Invalid email"),
  body("body").isLength({ min: 1 }).withMessage("Body is required"),
  (serverRequest, serverResponse) => {
    // Check any body errors
    const errors = validationResult(serverRequest);
    if (!errors.isEmpty()) {
      return serverResponse.status(400).json({ errors: errors.array() });
    }

    // Send the message to the API
    needle.post(
      `${API_URL}/contact_messages`,
      {
        email: serverRequest.body.email,
        body: serverRequest.body.body,
      },
      { json: true },
      (error, response) => {
        console.log(response.statusCode, error);
        if (error || response.statusCode >= 400) {
          console.log("error : ", error, "response : ", response);
          // if dev env
          if (process.env.NODE_ENV !== "env") {
            serverResponse.status(500).send(response.body);
          } else {
            serverResponse.status(500).send("Internal Server Error");
          }
        } else {
          serverResponse.status(200).send({
            message: "Message sent",
          });
        }
      }
    );
  }
);

module.exports = router;
